<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Stock_statusController
 *
 * @author  The scaffold-interface created at 2016-02-15 06:40:53am
 * @link  https://github.com/amranidev/scaffold-interfac
 */
class Stock_status extends Model
{

	protected $fillable = ['code','description','is_active'];  
    protected $guarded  = ['id'];
    public $timestamps = true;
    protected $dates = ['created_at', 'updated_at'];
	
}
