<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalesDetail extends Model
{
	protected $fillable = ['book','sales_id','qty','sub_total'];  
    protected $guarded  = ['id'];
    public $timestamps = true;
    protected $dates = ['created_at', 'updated_at'];
    public function Sales()
    {
        return $this->belongsTo('App\Models\Sales','lending_id');
    }
    public function Book()
    {
        return $this->belongsTo('App\Models\Book','book');
    }
}
