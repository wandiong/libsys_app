<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
	protected $fillable = ['code','description','is_active'];  
    protected $guarded  = ['id'];
    public $timestamps = true;
    protected $dates = ['created_at', 'updated_at'];
    public function Lending()
    {
    	return $this->hasMany('App\Models\Lending','id');
    }
}
