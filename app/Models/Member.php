<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
	protected $fillable = ['member_id','member_type'];  
    protected $guarded  = ['id'];
    public $timestamps = true;
    protected $dates = ['created_at', 'updated_at'];
    public function Lending()
    {
    	return $this->hasMany('App\Models\Lending','id');
    }
    public function Sales()
    {
        return $this->hasMany('App\Models\Sales','id');
    }    
    public function MemberType()
    {
    	return $this->belongsTo('App\Models\MemberType','member_type');
    }
}
