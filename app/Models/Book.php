<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
	protected $fillable = ['book_tittle','book_desc','is_active','categories','isbn','for_sale','qty','price','issue_date'];  
    protected $guarded  = ['id'];
    public $timestamps = true;
    protected $dates = ['created_at', 'updated_at'];

    public function Category()
    {
        return $this->belongsTo('App\Models\Category','categories');
    }
    public function Detail()
    {
        return $this->hasMany('App\Models\Detail','id');
    }
    public function SalesDetail()
    {
        return $this->hasMany('App\Models\SalesDetail','id');
    }
}
