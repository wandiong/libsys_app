<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Detail extends Model
{
 	protected $fillable = ['lending_id','book','status','total','is_active','status'];  
    protected $guarded  = ['id'];
    public $timestamps = true;
    protected $dates = ['created_at', 'updated_at','time_start','due_time'];
    public function Lending()
    {
        return $this->belongsTo('App\Models\Lending','lending_id');
    }
    public function Book()
    {
        return $this->belongsTo('App\Models\Book','book');
    }
    public function ReturnStatus()
    {
        return $this->belongsTo('App\Models\ReturnStatus','status');
    }

}
