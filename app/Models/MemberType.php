<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberType extends Model
{
	protected $fillable = ['code','description','member_price','is_active'];  
    protected $guarded  = ['id'];
    public $timestamps = true;
    protected $dates = ['created_at', 'updated_at'];
    public function Member()
    {
    	return $this->hasMany('App\Models\Member','id');
    }
}
