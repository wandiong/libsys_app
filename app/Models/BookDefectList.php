<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookDefectList extends Model
{
	protected $fillable = ['book_tittle','book_desc','is_active','categories','isbn','for_sale','qty','price','issue_date'];  
	protected $guarded  = ['id'];
	public $timestamps = true;
	protected $dates = ['created_at', 'updated_at'];
}
