<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccessRight extends Model
{
	protected $fillable = ['code','description','is_active'];  
    protected $guarded  = ['id'];
    public $timestamps = true;
    protected $dates = ['created_at', 'updated_at'];
  public function User()
    {
        return $this->hasMany('App\Models\User','id');
    }
}
