<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lending extends Model
{
	protected $fillable = ['is_member','member','customer','total','payment_method'];  
    protected $guarded  = ['id'];
    public $timestamps = true;
    protected $dates = ['created_at', 'updated_at'];
    public function Detail()
    {

        return $this->hasMany('App\Models\Detail','id');
    }
    public function Member()
    {
    	return $this->belongsTo('App\Models\Member','member');
    }
    public function PaymentMethod()
    {
        return $this->belongsTo('App\Models\PaymentMethod','payment_method');
    }
}
