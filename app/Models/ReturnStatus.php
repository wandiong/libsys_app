<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReturnStatus extends Model
{
	protected $fillable = ['code','description','member_price','is_active'];  
    protected $guarded  = ['id'];
    public $timestamps = true;
    protected $dates = ['created_at', 'updated_at'];
    public function Lending()
    {
    	return $this->hasMany('App\Models\Lending');
    }
    public function Detail()
    {
        return $this->hasMany('App\Models\ReturnStatus','id');
    }
}
