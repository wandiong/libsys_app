<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Illuminate\Contracts\Auth\Guard;

class LibrarianAuthenticate
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //if librarian
        if (Auth::check() && Auth::user()->access_right == 3)
        {
            return $next($request);
        }
        //if admin
        else if (Auth::check() && Auth::user()->access_right == 2)
        {
            return redirect('admin_panel');
        }
        //if superuser
        else if (Auth::check() && Auth::user()->access_right == 1)
        {
            return redirect('index');
        }
        return redirect()->guest('/login');
    }
}
