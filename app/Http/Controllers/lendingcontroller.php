<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Lending;
use App\Models\Detail;
use App\Models\ReturnStatus;


use Carbon\Carbon;
use DB;
use Session;
use View;
use Input;
use App;
use PDF;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
class LendingController extends Controller
{
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            $list = Lending::where('lendings.is_active', '=', 1)->get();
            return view('lendings.index')->with('list',$list);
        }
        public function viewcreate()
        {
            $data = new Lending();
            $temp = DB::table('members')->where('is_active','=',1)->get();
            $pm = DB::table('payment_methods')->where('is_active','=',1)->get();
            return view('lendings.create_form')->with('data',$data)
            ->with('temp',$temp)
            ->with('pm',$pm);
        }
        public function create(request $request)
        {
            $obj = new Lending();
            $obj->member = $request->member_id;
            $obj->customer = $request->customer;
            $obj->payment_method = $request->payment_method;
            $obj->total = 0;
            $obj->is_active = true;
            $obj->is_member = $request->is_member;
            $obj->created_at = Carbon::now()->toDateTimeString();
            $obj->updated_at = null;
            $obj->save();
            Session::flash("messages", "Save Success !");
            return redirect('/lending');
        }
        public function edit($id)
        {
            $data = Lending::find($id);
            $temp = DB::table('members')->where('is_active','=',1)->get();
            $pm = DB::table('payment_methods')->where('is_active','=',1)->get();
            if($data == null)
            {
                return redirect('error/503');
            }
            else
            {
                return View::make('lendings.update_form')
                ->with('data',$data)
                ->with('temp',$temp)
                ->with('pm',$pm);
            }
        }
        public function update(request $request)
        {
            $Lending                                   = Lending::find($request->id);
                        //for avoiding data not shown, I think only this way
            $Lending->member                           = 1;
            $Lending->customer                         = $request->customer;
            $Lending->payment_method                   = $request->payment_method;
            $Lending->total                            = $request->total;
            $Lending->updated_at                       = Carbon::now()->toDateTimeString();
            $Lending->save();
            Session::flash('message', 'Successfully updated');
            return redirect('/lending');
        }
        public function destroy($id)
        {
            $lending             = Lending::find($id);
            $lending->is_active  = false;
            $lending->updated_at = Carbon::now()->toDateTimeString();
            $lending->save();
            Session::flash('message', 'Successfully Deleted');
            return redirect('/lending');
        }
        public function indexDetail($id)
        {
            $detail = Detail::where('details.is_active','=',1)
            ->where('lending_id','=',$id)
            ->get();
            $count = Detail::where('is_active','=',1)->where('lending_id','=',$id)->count();
            $overview = Lending::where('lendings.id','=',$id)
            ->get();
            $stat = DB::table('return_statuses')->where('is_active','=',1)->get();
            return view('details.index')
            ->with('detail',$detail)
            ->with('overview',$overview)
            ->with('stat',$stat)
            ->with('count',$count);
        }
        public function showcreateDetail($id)
        {
           $data = new Detail();
           $lending = Lending::where('id','=',$id)->get();
           $temp = DB::table('books')->where('is_active','=',1)->get();
           return view('details.create_form')
           ->with('data',$data)
           ->with('temp',$temp)
           ->with('lending',$lending);
       }
       public function createDetail(request $request)
       {
        $obj = new Detail($request->all());
        $obj->book = $request->book;
        $obj->lending_id = $request->lending_id;
        $obj->due_time = $request->due_time;
        $obj->time_start = $request->time_start;
        $obj->status = 1;
        $obj->total = 0;
        $obj->is_active = true;
        $obj->created_at = Carbon::now()->toDateTimeString();
        $obj->updated_at = null;
        $obj->save();
        Session::flash("messages", "Save Success !");
        return redirect('/details/'.$request->lending_id);   
    }
    public function showedit($id)
    {
        $obj = Detail::find($id);
        return view('details.update_status')->with('obj',$obj);
    }
    public function updateStatus(request $request)
    {
        $obj = Detail::find($request->id);
        $obj->status = $request->status;
        $overview = Lending::where('lendings.id','=',$request->main_id)
                    ->first();
        //trying to differs due time and present time which customer returned the book
        $difference = Carbon::now()->diffInDays($obj->due_time);
        //if following customer is member
            //if following customer is native customer
        if ($overview->is_member == 0)
        {
                //if overdued
            if ($obj->status == 4)
            {
                if ($difference <= 1)
                {
                    $obj->total = 20000;
                }
                else
                {
                    $obj->total = $difference * 20000;
                }        
            }
            else if ($obj->status == 3)
            {
                $obj->total = 20000;
            }
        }
        $obj->save();

        $objOver = Lending::find($request->main_id);
        $objTotal = DB::table('details')
                ->where('lending_id','=', $request->main_id)
                ->sum('total');
        $objOver->total = $objTotal;
        $objOver->save();

        Session::flash("messages", "Update Success !");
        return redirect('/details/'.$request->main_id);   
    }
    public function delete(Request $request,$id)
    {
        $lending             = Detail::find($id);
        $lending->is_active  = false;
        $lending->updated_at = Carbon::now()->toDateTimeString();
        $lending->save();
        Session::flash('message', 'Successfully Deleted');
        return redirect('/details/'.$lending->lending_id);   
    }
    public function pdf($id) 
    {
        $detail = Detail::where('lending_id','=',$id)->where('is_active','=',1)->get();
        $overview = Lending::join('members','lendings.member','=','members.id')
        ->join('payment_methods','lendings.payment_method','=','payment_methods.id')
        ->select('lendings.is_member as membership',
            'payment_methods.code as payment_method',
            'members.member_id as member',
            'lendings.customer as customer',
            'lendings.created_at as created_date',
            'lendings.total as fee',
            'lendings.id as main_id')
        ->where('lendings.id','=',$id)
        ->get();
        $view = View::make('details.pdf', array('detail' => $detail,'overview' => $overview))->render(); 
        $pdf = \PDF::loadHTML($view);
        return $pdf->stream();
    }
}
