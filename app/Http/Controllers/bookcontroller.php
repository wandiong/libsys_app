<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Book;
use App\Models\Category;
use Carbon\Carbon;
use DB;
use Session;
use View;
use Input;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Flash;
class BookController extends controller
{
	public function showBook()
	{
		$list = Book::where('is_active', '=', 1)
		->paginate(20);
		return view('books.index')->with('list',$list);
	}
	public function showNewBook()
	{
		$objcat = DB::table('categories')->where('is_active','=',1)->get();
		$obj = new Book();
		return view('books.create_form')->with('obj',$obj)
		->with('temp',$objcat);
	}
	public function createNewBook(Request $request)
	{
		$obj = new Book($request->all());
		$obj->is_active = true;
		$obj->issue_date = $request->issue_date;
		$obj->categories = $request->categories;
		$obj->for_sale = $request->sell;
		$obj->created_at = Carbon::now()->toDateTimeString();
		$obj->updated_at = null;
		$obj->save();
		Flash::success("Save Success !");
		return redirect('/book');
	}
	public function showEditBook($id)
	{

		$objcat = DB::table('categories')->where('is_active','=',1)->get();
		$objBook = Book::find($id);
		if($objBook == null)
		{
			return redirect('error/503');
		}else{
			return View::make('books.update_form')
			->with('obj',$objBook)
			->with('temp',$objcat);
		}
	}
	public function UpdateBook(request $request)
	{
		$temp = Input::get('id');
		$Book 					 	 		 	 = Book::find($temp);
		$Book->issue_date 						 = $request->issue_date;
		$Book->isbn 							 = $request->isbn;
		$Book->qty 							 	 = $request->qty;
		$Book->price 							 = $request->price;
		$Book->book_tittle 			 		 	 = $request->book_tittle;
		$Book->book_desc 			 		 	 = $request->book_desc;
		$Book->categories 			 		 	 = $request->categories;
		$Book->updated_at      	 	 		 	 = Carbon::now()->toDateTimeString();
		$Book->save();
		Flash::success('Successfully updated');
		return redirect('/book');
	}
	public function deleteBook($id)
	{
		$Book  		      = Book::find($id);
		$Book->is_active  = false;
		$Book->updated_at = Carbon::now()->toDateTimeString();
		$Book->save();
		Flash::success('Successfully Deleted');
		return redirect('/book');
	}
}

?>