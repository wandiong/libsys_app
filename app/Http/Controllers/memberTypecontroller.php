<?php
	namespace App\Http\Controllers;
	use Illuminate\Http\Request;
	use App\Models\MemberType;
	use Carbon\Carbon;
	use DB;
	use Session;
	use View;
	use Input;
	use Illuminate\Pagination\LengthAwarePaginator;
	use Illuminate\Pagination\Paginator;
	class MemberTypeController extends controller
	{
		public function showMemberType()
			{
				$list = MemberType::where('is_active', '=', 1)->paginate(15);
				
				return view('member_types.index')->with('list',$list);
			}
		public function showNewMemberType()
			{
				$obj = new MemberType();
				return view('member_types.create_form')->with('obj',$obj);
			}
		public function createNewMemberType(Request $request)
			{
					$obj = new MemberType($request->all());
					$obj->lending_limit = $request->lending_limit;
					$obj->is_active = true;
					$obj->created_at = Carbon::now()->toDateTimeString();
					$obj->updated_at = null;
					$obj->save();
					Session::flash("messages", "Save Success !");
					return redirect('/member_type');
	        }
		public function showEditMemberType($id)
			{

		        $objMemberType = MemberType::find($id);
				if($objMemberType == null)
				{
					return redirect('error/503');
				}else{
		        	return View::make('member_types.update_form')
		            ->with('obj',$objMemberType);
		        }
			}
	   	public function UpdateMemberType(request $request)
			{
					$temp = Input::get('id');
		            $memberType 					 	 		 = MemberType::find($temp);
					$memberType->code 			 		 		 = $request->code;
					$memberType->description 			 		 = $request->description;
					$memberType->member_price 			 		 = $request->member_price;
		            $memberType->updated_at      	 	 		 = Carbon::now()->toDateTimeString();
		            $memberType->lending_limit					 = $request->lending_limit;
		            $memberType->save();
		            Session::flash('message', 'Successfully updated');
					return redirect('/member_type');
			}
		public function deleteMemberType($id)
			{
				$memberType  		   = MemberType::find($id);
				$memberType->is_active = false;
				$memberType->updated_at = Carbon::now()->toDateTimeString();
				$memberType->save();
			    Session::flash('message', 'Successfully Deleted');
				return redirect('/member_type');
			}
	}

?>