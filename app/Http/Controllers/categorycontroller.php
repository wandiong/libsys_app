<?php
	namespace App\Http\Controllers;
	use Illuminate\Http\Request;
	use App\Models\Category;
	use Carbon\Carbon;
	use DB;
	use Session;
	use View;
	use Input;
	use Illuminate\Pagination\LengthAwarePaginator;
	use Illuminate\Pagination\Paginator;
	class CategoryController extends controller
	{
		public function showCategory()
			{
				$list = Category::where('is_active', '=', 1)->paginate(15);
				return view('Categories.index')->with('list',$list);
			}
		public function showNewCategory()
			{
				$obj = new Category();
				return view('Categories.create_form')->with('obj',$obj);
			}
		public function createNewCategory(Request $request)
			{
					$obj = new Category($request->all());
					$obj->is_active = true;
					$obj->created_at = Carbon::now()->toDateTimeString();
					$obj->updated_at = null;
					$obj->save();
					Session::flash("messages", "Save Success !");
					return redirect('/category');
	        }
		public function showEditCategory($id)
			{

		        $objCategory = Category::find($id);
				if($objCategory == null)
				{
					return redirect('error/503');
				}else{
		        	return View::make('Categories.update_form')
		            ->with('obj',$objCategory);
		        }
			}
	   	public function UpdateCategory(request $request)
			{
					$temp = Input::get('id');
		            $Category 					 	 		 	 = Category::find($temp);
					$Category->code 			 		 		 = $request->code;
					$Category->description 			 		 = $request->description;
		            $Category->updated_at      	 	 		 = Carbon::now()->toDateTimeString();
		            $Category->save();
		            Session::flash('message', 'Successfully updated');
					return redirect('/category');
			}
		public function deleteCategory($id)
			{
				$Category  		   = Category::find($id);
				$Category->is_active  = false;
				$Category->updated_at = Carbon::now()->toDateTimeString();
				$Category->save();
			    Session::flash('message', 'Successfully Deleted');
				return redirect('/category');
			}
	}

?>