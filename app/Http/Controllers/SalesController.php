<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Sales;
use App\Models\SalesDetail;
use App\Models\Member;
use App\Models\Book;
use App\Models\PaymentMethod;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Flash;
use Carbon\Carbon;
use DB;
use PDF;
use View;
class SalesController extends Controller
{
//sales function
  public function index()
  {
    $list = Sales::where('is_active','=',1)->get();
    return view('sales.index')->with('list',$list);
  }

  public function create()
  {
    $create = new Sales();
    $member = Member::where('is_active','=',1)->get();
    $payment_method = PaymentMethod::where('is_active','=',1)->get();
    return view('sales.create_form')->with('create',$create)
    ->with('member',$member)
    ->with('payment_method',$payment_method);
  }

  public function store(Request $request)
  {
    $obj = new Sales();
    $obj->sales_date = $request->sales_date;
    $obj->member = $request->member_id;
    $obj->customer = $request->customer;
    $obj->payment_method = $request->payment_method;
    $obj->grand_total = 0;
    $obj->is_active = true;
    $obj->is_member = $request->is_member;
    $obj->created_at = Carbon::now()->toDateTimeString();
    $obj->updated_at = null;
    $obj->save();
    Flash::message("Save Success !");
    return redirect('/sales');
  }

  public function edit($id)
  {
    $edit = Sales::find($id);
    $member = Member::where('is_active','=',1)->get();
    $payment_method = PaymentMethod::where('is_active','=',1)->get();
    return view('sales.update_form')->with('edit',$edit)
    ->with('member',$member)
    ->with('payment_method',$payment_method);
  }

  public function update(Request $request)
  {
    $Sales                                   = Sales::find($request->id);
//code ref = Lending controller, since by UI lending and sales are same but in flow , lending is meant for book status= for lend , and sales is meant for book status= for sale
    $Sales->member                           = $request->member_code;
    $Sales->customer                         = $request->customer;
    $Sales->payment_method                   = $request->payment_method;
    $Sales->grand_total                      = $request->total;
    $Sales->updated_at                       = Carbon::now()->toDateTimeString();
    $Sales->save();
    Flash::success('Successfully updated');
    return redirect('/sales');
  }

  public function destroy($id)
  {
    $delete = Sales::find($id);
    $delete->is_active = 0;
    $delete->save();
    Flash::success('Successfully Deleted');
    return redirect('/sales');       
  }

//sales detail function
  public function indexDetail($id)
  {
    $overview = Sales::where('id','=',$id)->get();
    $detail = SalesDetail::where('is_active','=',1)->where('sales_id','=',$id)->get();
    $book = Book::where('is_active','=',1)->where('qty','>',0)->where('for_sale','=',1)->get();
    return view('sales_details.index')->with('detail',$detail)
    ->with('book',$book)
    ->with('overview',$overview);
  }
  public function storeDetail(Request $request)
  {
    $cost = Book::where('id','=',$request->book)->first()->price;
    $obj = new SalesDetail();
    $obj->sales_id = $request->id;
    $obj->book = $request->book;
    $obj->qty = $request->qty;
    $obj->price = $cost;
    $obj->sub_total = $request->qty * $cost;
    $obj->is_active = 1;
    $obj->save();
    $reduce_book = Book::where('id','=',$request->book)->first();
    if ($request->qty > $reduce_book->qty)
    {
      Flash::error('your stock is exceeded, please inform to customer/member if the stock just has '.$reduce_book.' remaining');
      return redirect('/sales_detail/'.$obj->sales_id);
    }else{
      $reduce_book->qty = $reduce_book->qty - $request->qty ;
      $reduce_book->save(); 
      $sum = SalesDetail::where('is_active','=',1)->where('sales_id','=',$request->id)->sum('sub_total');
      $get_total = Sales::where('id','=',$request->id)->first();
      $get_total->grand_total = $sum;
      $get_total->save();
      Flash::success("Save Success !");
      return redirect('/sales_detail/'.$obj->sales_id);
    }
  }
  public function destroyDetail($id)
  {
    $delete = SalesDetail::find($id);
    $delete->is_active = 0;
    $delete->save();
    $total = Sales::where('id','=',$delete->sales_id)->first();
    $total->grand_total = $total->grand_total - $delete->sub_total;
    $total->save();
    $add = Book::where('id','=',$delete->book)->first();
    $add->qty = $add->qty + $delete->qty;
    $add->save();
    Flash::success('Successfully Deleted');
    return redirect('/sales_detail/'.$delete->sales_id);    
  }
  public function pdf($id) 
  {
    $detail = SalesDetail::where('sales_id','=',$id)->where('is_active','=',1)->get();
    $overview = Sales::where('id','=',$id)->get();
    $view = View::make('sales_details.pdf', array('detail' => $detail,'overview' => $overview))->render(); 
    $pdf = \PDF::loadHTML($view);
    return $pdf->stream();
  }
}
