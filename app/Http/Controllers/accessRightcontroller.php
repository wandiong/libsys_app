<?php
	namespace App\Http\Controllers;
	use Illuminate\Http\Request;
	use App\Models\AccessRight;
	use Carbon\Carbon;
	use DB;
	use Session;
	use View;
	use Input;
	use Illuminate\Pagination\LengthAwarePaginator;
	use Illuminate\Pagination\Paginator;
	class AccessRightController extends controller
	{
		public function showAccessRight()
			{
				$list = AccessRight::where('is_active', '=', 1)->paginate(15);
				return view('access_rights.index')->with('list',$list);
			}
		public function showNewAccessRight()
			{
				$obj = new AccessRight();
				return view('access_rights.create_form')->with('obj',$obj);
			}
		public function createNewAccessRight(Request $request)
			{
					$obj = new AccessRight($request->all());
					$obj->is_active = true;
					$obj->created_at = Carbon::now()->toDateTimeString();
					$obj->updated_at = null;
					$obj->save();
					Session::flash("messages", "Save Success !");
					return redirect('/access_right');
	        }
		public function showEditAccessRight($id)
			{

		        $objAccessRight = AccessRight::find($id);
				if($objAccessRight == null)
				{
					return redirect('error/503');
				}else{
		        	return View::make('access_rights.update_form')
		            ->with('data',$objAccessRight);
		        }
			}
	   	public function UpdateAccessRight(request $request)
			{
					$temp = Input::get('id');
		            $AccessRight 					 	 		 	 = AccessRight::find($temp);
					$AccessRight->code 			 		 		 = $request->code;
					$AccessRight->description 			 		 = $request->description;
		            $AccessRight->updated_at      	 	 		 = Carbon::now()->toDateTimeString();
		            $AccessRight->save();
		            Session::flash('message', 'Successfully updated');
					return redirect('/access_right');
			}
		public function deleteAccessRight($id)
			{
				$AccessRight  		   = AccessRight::find($id);
				$AccessRight->is_active  = false;
				$AccessRight->updated_at = Carbon::now()->toDateTimeString();
				$AccessRight->save();
			    Session::flash('message', 'Successfully Deleted');
				return redirect('/access_right');
			}

	}

?>