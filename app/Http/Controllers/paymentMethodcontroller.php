<?php
	namespace App\Http\Controllers;
	use Illuminate\Http\Request;
	use App\Models\PaymentMethod;
	use Carbon\Carbon;
	use DB;
	use Session;
	use View;
	use Input;
	use Illuminate\Pagination\LengthAwarePaginator;
	use Illuminate\Pagination\Paginator;
	class PaymentMethodController extends controller
	{
		public function showPaymentMethod()
			{
				$list = PaymentMethod::where('is_active', '=', 1)->paginate(15);
				;
				return view('payment_methods.index')->with('list',$list);
			}
		public function showNewPaymentMethod()
			{
				$obj = new PaymentMethod();
				return view('payment_methods.create_form')->with('obj',$obj);
			}
		public function createNewPaymentMethod(Request $request)
			{
					$obj = new PaymentMethod($request->all());
					$obj->is_active = true;
					$obj->created_at = Carbon::now()->toDateTimeString();
					$obj->updated_at = null;
					$obj->save();
					Session::flash("messages", "Save Success !");
					return redirect('/payment_method');
	        }
		public function showEditPaymentMethod($id)
			{

		        $objPaymentMethod = PaymentMethod::find($id);
				if($objPaymentMethod == null)
				{
					return redirect('error/503');
				}else{
		        	return View::make('payment_methods.update_form')
		            ->with('obj',$objPaymentMethod);
		        }
			}
	   	public function UpdatePaymentMethod(request $request)
			{
					$temp = Input::get('id');
		            $PaymentMethod 					 	 		 	 = PaymentMethod::find($temp);
					$PaymentMethod->code 			 		 		 = $request->code;
					$PaymentMethod->description 			 		 = $request->description;
		            $PaymentMethod->updated_at      	 	 		 = Carbon::now()->toDateTimeString();
		            $PaymentMethod->save();
		            Session::flash('message', 'Successfully updated');
					return redirect('/payment_method');
			}
		public function deletePaymentMethod($id)
			{
				$PaymentMethod  		   = PaymentMethod::find($id);
				$PaymentMethod->is_active  = false;
				$PaymentMethod->updated_at = Carbon::now()->toDateTimeString();
				$PaymentMethod->save();
			    Session::flash('message', 'Successfully Deleted');
				return redirect('/payment_method');
			}
	}

?>