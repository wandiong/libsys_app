<?php
	namespace App\Http\Controllers;
	use Illuminate\Http\Request;
	use App\Models\ReturnStatus;
	use Carbon\Carbon;
	use DB;
	use Session;
	use View;
	use Input;
	use Illuminate\Pagination\LengthAwarePaginator;
	use Illuminate\Pagination\Paginator;
	class ReturnStatusController extends controller
	{
		public function showReturnStatus()
			{
				$list = ReturnStatus::where('is_active', '=', 1)->paginate(15);
				return view('return_statuses.index')->with('list',$list);
			}
		public function showNewReturnStatus()
			{
				$obj = new ReturnStatus();
				return view('return_statuses.create_form')->with('obj',$obj);
			}
		public function createNewReturnStatus(Request $request)
			{
					$obj = new ReturnStatus($request->all());
					$obj->is_active = true;
					$obj->created_at = Carbon::now()->toDateTimeString();
					$obj->updated_at = null;
					$obj->save();
					Session::flash("messages", "Save Success !");
					return redirect('/return_status');
	        }
		public function showEditReturnStatus($id)
			{

		        $objReturnStatus = ReturnStatus::find($id);
				if($objReturnStatus == null)
				{
					return redirect('error/503');
				}else{
		        	return View::make('return_statuses.update_form')
		            ->with('obj',$objReturnStatus);
		        }
			}
	   	public function UpdateReturnStatus(request $request)
			{
					$temp = Input::get('id');
		            $ReturnStatus 					 	 		 	 = ReturnStatus::find($temp);
					$ReturnStatus->code 			 		 		 = $request->code;
					$ReturnStatus->description 			 		 = $request->description;
		            $ReturnStatus->updated_at      	 	 		 = Carbon::now()->toDateTimeString();
		            $ReturnStatus->save();
		            Session::flash('message', 'Successfully updated');
					return redirect('/return_status');
			}
		public function deleteReturnStatus($id)
			{
				$ReturnStatus  		   = ReturnStatus::find($id);
				$ReturnStatus->is_active  = false;
				$ReturnStatus->updated_at = Carbon::now()->toDateTimeString();
				$ReturnStatus->save();
			    Session::flash('message', 'Successfully Deleted');
				return redirect('/return_status');
			}
	}

?>