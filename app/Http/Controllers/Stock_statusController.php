<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Stock_Status;
use Carbon\Carbon;
use DB;
use Session;
use View;
use Input;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Flash;

class Stock_statusController extends Controller
{

    public function index()
    {
        $list = Stock_Status::where('is_active','=',1)->get();
        return view('stock_statuses.index')->with('list',$list);
    }

    public function create()
    {
        $stock_status = new Stock_Status();
        return view('stock_statuses.create_form')->with('stock_status',$stock_status);
    }

    public function store(Request $request)
    {
        $obj = new Stock_Status($request->all());
        $obj->is_active = true;
        $obj->created_at = Carbon::now()->toDateTimeString();
        $obj->updated_at = Carbon::now()->toDateTimeString();
        $obj->save();
        Flash::success("Save ".$obj->code." Success !");
        return redirect('/stock_status');
    }
    public function edit($id)
    {                   
        $stock_status = Stock_Status::find($id);
        if($stock_status == null)
        {
            return redirect('error/503');
        }else{
            return View::make('stock_statuses.update_form')
            ->with('stock_status',$stock_status);
        }
    }
    public function update(request $request)
    {
        $stock_status                                = Stock_Status::find($request->id);
        $stock_status->code                          = $request->code;
        $stock_status->description                   = $request->description;
        $stock_status->updated_at                    = Carbon::now()->toDateTimeString();
        $stock_status->save();
        Flash::Success('Successfully updated');
        return redirect('/stock_status');
    }
    public function destroy($id)
    {
        $stock_status          = Stock_Status::find($id);
        $stock_status->is_active  = false;
        $stock_status->updated_at = Carbon::now()->toDateTimeString();
        $stock_status->save();
        Flash::success('Successfully Deleted');
        return redirect('/stock_status');
    }

}
