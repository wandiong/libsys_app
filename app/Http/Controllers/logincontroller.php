<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use App\Models\User;
use session;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Flash;
class LoginController extends controller
{
	public function showLogin()
	{
		return view('login');
	}
	public function doLogin(Request $request)
	{
			//if account is exists
		if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) 
		{
				// if accout is "deleted" or not
			if(Auth::user()->is_active == 1)
			{
			    	//dd('success');
				if (Auth::user()->access_right == 1)
				{
					//super user access
					Flash::message('Welcome Aboard!'.' '.Auth::user()->name);
					return redirect('index');
				}
				else if (Auth::user()->access_right == 2)
				{
					//admin access
					Flash::message('Welcome Aboard!'.' '.Auth::user()->name);
					return redirect('admin_panel');
				}
				else if (Auth::user()->access_right == 3)
				{
					//librarian access
					Flash::message('Welcome Aboard!'.' '.Auth::user()->name);
					return redirect('librarian_panel');
				}
			}
			else
			{
		    		//return redirect()->intended('common');
			}
		}
		else
		{
			Flash::error('Invalid Username/ Password');
			return view('login');
		}
	}
       	//logout
	public function logout()
	{
		Auth::logout();
		return redirect('login');	
	}
	public function SuccessLogin()
	{
		return view('superuser.index');
	}
	public function SuccessLoginAdmin()
	{
		return view('admin.index');
	}
	public function SuccessLoginLibrarian()
	{
		return view('librarian.index');
	}

}
?>