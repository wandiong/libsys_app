<?php
	namespace App\Http\Controllers;
	use Illuminate\Http\Request;
	use App\Models\User;
	use App\Models\AccessRight;
	use Carbon\Carbon;
	use DB;
	use Session;
	use View;
	use Input;
	use Hash;
	use Illuminate\Pagination\LengthAwarePaginator;
	use Illuminate\Pagination\Paginator;
	class UserController extends controller
	{
		public function showUser()
			{
				$list = User::paginate(15);
				$temp = DB::table('access_rights')->select('id', 'code')->where('is_active','=',1)->get();
				return view('users.index')->with('list',$list)
										  ->with('temp',$temp);
			}
		public function showNewUser()
			{
				$obj = new User();
				$temp = DB::table('access_rights')->select('id', 'code')->where('is_active','=',1)->get();
				return view('users.create_form')->with('obj',$obj)
												->with('temp',$temp);
			}
		public function createNewUser(Request $request)
			{
					$obj = new User($request->all());
					$obj->access_right = $request->access_right;
					$obj->password = Hash::make($request->password);
					$obj->is_active = false;
					$obj->created_at = Carbon::now()->toDateTimeString();
					$obj->updated_at = null;
					$obj->save();
					Session::flash("messages", "Save Success !");
					return redirect('/user');
	        }
		public function showEditUser($id)
			{

		        $objUser = User::find($id);
				$temp = DB::table('access_rights')->select('id', 'code')->where('is_active','=',1)->get();
				if($objUser == null)
				{
					return redirect('error/503');
				}else{
		        	return View::make('users.update_form')
		            ->with('obj',$objUser)
		            ->with('temp',$temp);
		        }
			}
	   	public function UpdateUser(request $request)
			{
					$temp = Input::get('id');
//		            dd($request->name);
		            $User 					 	 		 = User::find($temp);
					$User->name 							 = $request->name;
					$User->email 						 = $request->email;
					$User->access_right 					 = $request->access_right;
					$User->password 						 = Hash::make($request->password);
					$User->updated_at      	 	 		 = Carbon::now()->toDateTimeString();
		            $User->save();
		            Session::flash('message', 'Successfully updated');
					return redirect('/user');
			}
		public function deleteUser($id)
			{
				$User  		   	  = User::find($id);
				if ($User->is_active == 1)
				{
					$User->is_active  = false;
				}else{
					$User->is_active = true;
				}
				$User->updated_at = Carbon::now()->toDateTimeString();
				$User->save();
			    Session::flash('message', 'Successfully Updated User Status');
				return redirect('/user');
			}
		public function updateAccessRight(request $request)
			{
					$temp				= User::find(Input::get('id'));
					$temp->access_right = $request->input('access_right');
					$temp->save();
				    Session::flash('message', 'Successfully Updated');
					return redirect('/user');					
			}
	}

?>