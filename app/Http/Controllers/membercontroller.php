<?php
	namespace App\Http\Controllers;
	use Illuminate\Http\Request;
	use App\Models\Member;
	use App\Models\MemberType;
	use Carbon\Carbon;
	use DB;
	use Session;
	use View;
	use Input;
	class MemberController extends controller
	{
		public function showMember()
			{
				$list = Member::join('member_types','members.member_type','=','member_types.id')
																	->where('members.is_active','=',1)
																	->select('members.member_id as member_id',
																			 'member_types.code as member_type',
																			 'members.id as id')
																	->paginate(15);
				return view('members.index')->with('list',$list);
			}
		public function showNewMember()
			{
				$rand = rand(10000000, 99999999999);
				$obj = new Member();
				$temp = DB::table('member_types')->where('is_active', '=', 1)->get();
				return view('members.create_form')->with('obj',$obj)
												   ->with('temp',$temp)
												   ->with('rand',$rand);
			}
		public function createNewMember(Request $request)
			{
					$obj = new Member($request->all());
					$obj->member_type = $request->member_type;
					$obj->member_id = $request->member_code;
					$obj->is_active = true;
					$obj->created_at = Carbon::now()->toDateTimeString();
					$obj->updated_at = null;
					$obj->save();
					Session::flash("messages", "Save Success !");
					return redirect('/member');
	        }
		public function showEditMember($id)
			{
		    	$objMember = Member::find($id);
				$objMemberType = DB::table('member_types')->where('is_active', '=', 1)->get();
		       	if($objMember == null)
				{
					return redirect('error/503');
				}else{
			    return View::make('members.update_form')
			            ->with('objMember', $objMember)
			            ->with('objMemberType',$objMemberType);
			    }
			}
	   	public function UpdateMember(request $request)
			{
					$temp = Input::get('id');
		            $member 					 	 = Member::find($temp);
					$member->member_type 			 = $request->member_type;
		            $member->updated_at      	 	 = Carbon::now()->toDateTimeString();
		            $member->save();
		            Session::flash('message', 'Successfully updated');
					return redirect('/member');
			}
		public function deleteMember($id)
			{
				$member  = Member::find($id);
				$member->is_active = false;
				$member->updated_at = Carbon::now()->toDateTimeString();
				$member->save();
			    Session::flash('message', 'Successfully Deleted');
				return redirect('/member');
			}
	}

?>