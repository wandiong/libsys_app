<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/login', 'LoginController@showLogin');
Route::get('/logout', 'LoginController@logout');
Route::POST('/login', 'LoginController@doLogin');
route::get('index','LoginController@SuccessLogin');
route::get('admin_panel','LoginController@SuccessLoginAdmin');
route::get('librarian_panel','LoginController@SuccessLoginLibrarian');

	//superuser can access one

		//member
Route::get('/member','MemberController@showMember');
Route::get('/member/new','MemberController@showNewMember');
Route::POST('/member/insert','MemberController@createNewMember');
Route::get('/member/updating/{id}','MemberController@showEditMember');
Route::POST('/member/update','MemberController@UpdateMember');
Route::get('/member/drop/{id}','MemberController@deleteMember');

		//member_type
Route::get('/member_type','MemberTypeController@showMemberType');
Route::get('/member_type/new','MemberTypeController@showNewMemberType');
Route::POST('/member_type/insert','MemberTypeController@CreateNewMemberType');
Route::get('/member_type/updating/{id}','MemberTypeController@showEditMemberType');
Route::POST('/member_type/update','MemberTypeController@UpdateMemberType');
Route::get('/member_type/drop/{id}','MemberTypeController@deleteMemberType');

		//payment_method
Route::get('/payment_method','PaymentMethodController@showPaymentMethod');
Route::get('/payment_method/new','PaymentMethodController@showNewPaymentMethod');
Route::POST('/payment_method/insert','PaymentMethodController@createNewPaymentMethod');
Route::get('/payment_method/updating/{id}','PaymentMethodController@showEditPaymentMethod');
Route::POST('/payment_method/update','PaymentMethodController@UpdatePaymentMethod');
Route::get('/payment_method/drop/{id}','PaymentMethodController@deletePaymentMethod');

		//return_status
Route::get('/return_status','ReturnStatusController@showReturnStatus');
Route::get('/return_status/new','ReturnStatusController@showNewReturnStatus');
Route::POST('/return_status/insert','ReturnStatusController@createNewReturnStatus');
Route::get('/return_status/updating/{id}','ReturnStatusController@showEditReturnStatus');
Route::POST('/return_status/update','ReturnStatusController@UpdateReturnStatus');
Route::get('/return_status/drop/{id}','ReturnStatusController@deleteReturnStatus');

		//category
Route::get('/category','CategoryController@showCategory');
Route::get('/category/new','CategoryController@showNewCategory');
Route::POST('/category/insert','CategoryController@createNewCategory');
Route::get('/category/updating/{id}','CategoryController@showEditCategory');
Route::POST('/category/update','CategoryController@UpdateCategory');
Route::get('/category/drop/{id}','CategoryController@deleteCategory');

		//access_right
Route::get('/access_right','AccessRightController@showAccessRight');
Route::get('/access_right/new','AccessRightController@showNewAccessRight');
Route::POST('/access_right/insert','AccessRightController@createNewAccessRight');
Route::get('/access_right/updating/{id}','AccessRightController@showEditAccessRight');
Route::POST('/access_right/update','AccessRightController@UpdateAccessRight');
Route::get('/access_right/drop/{id}','AccessRightController@deleteAccessRight');

		//book
Route::get('/book','BookController@showBook');
Route::get('/book/new','BookController@showNewBook');
Route::POST('/book/insert','BookController@createNewBook');
Route::get('/book/updating/{id}','BookController@showEditBook');
Route::POST('/book/update','BookController@UpdateBook');
Route::get('/book/drop/{id}','BookController@deleteBook');

		//user
Route::get('/user','UserController@showUser');
Route::get('/user/new','UserController@showNewUser');
Route::POST('/user/insert','UserController@createNewUser');
Route::get('/user/updating/{id}','UserController@showEditUser');
Route::POST('/user/update','UserController@UpdateUser');
Route::get('/user/drop/{id}','UserController@deleteUser');
Route::resource('/user/bulkupdate', 'UserController@updateAccessRight');

		//lending
Route::get('/lending','LendingController@index');
Route::get('/lending/new','LendingController@viewcreate');
Route::POST('/lending/insert','LendingController@create');
Route::get('/lending/updating/{id}','LendingController@edit');
Route::POST('/lending/update','LendingController@update');
Route::get('/lending/drop/{id}','LendingController@destroy');
		//details	
Route::get('/details/{id}','LendingController@indexDetail');
Route::get('/details/new/{id}','LendingController@showcreateDetail');		
Route::POST('/details/create','LendingController@createDetail');	
Route::get('/details/updating/{id}','LendingController@showedit');
Route::get('/details/drop/{id}','LendingController@delete');		

		//stock_status	
Route::get('/stock_status','Stock_statusController@index');
Route::get('/stock_status/new','Stock_statusController@create');		
Route::POST('/stock_status/create','Stock_statusController@store');	
Route::get('/stock_status/updating/{id}','Stock_statusController@edit');
Route::POST('/stock_status/update','Stock_statusController@update');
Route::get('/stock_status/drop/{id}','Stock_statusController@destroy');		
		//sales
Route::get('/sales','SalesController@index');
Route::get('/sales/new','SalesController@create');		
Route::POST('/sales/insert','SalesController@store');	
Route::get('/sales/updating/{id}','SalesController@edit');
Route::POST('/sales/update','SalesController@update');
Route::get('/sales/drop/{id}','SalesController@destroy');		

		//detail
Route::get('/sales_detail/{id}','SalesController@indexDetail');
Route::POST('/sales_detail/create','SalesController@storeDetail');	
Route::get('/sales_detail/drop/{id}','SalesController@destroyDetail');	
Route::get('/sales_detail/generate_pdf/{id}','SalesController@pdf',function(){
	$pdf = PDF::loadView('sales_details.pdf');
	return $pdf-stream();
});	
Route::get('/details/generate_pdf/{id}','LendingController@pdf' ,function(){
	$pdf = PDF::loadView('details.pdf');;
	return $pdf->stream();
});

Route::get('error/503',function(){
	return view('errors.503');
});

Route::get('/', function () {
	return view('login');
});