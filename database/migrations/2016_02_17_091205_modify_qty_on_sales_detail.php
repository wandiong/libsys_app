<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyQtyOnSalesDetail extends Migration
{
/**
* Run the migrations.
*
* @return void
*/
public function up()
{
    Schema::table('sales_details', function($table)
    {
        $table->dropColumn('qty');
    });
}

/**
* Reverse the migrations.
*
* @return void
*/
public function down()
{
//
}
}
