<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLendings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lendings', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('is_active');
            $table->boolean('is_member');
            $table->integer('member')->unsigned()->nullable();
            $table->foreign('member')->references('id')->on('members');
            $table->string('customer');
            $table->integer('payment_method')->unsigned();
            $table->foreign('payment_method')->references('id')->on('payment_methods');
            $table->double('total',18,2);
            $table->timestamps();
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
