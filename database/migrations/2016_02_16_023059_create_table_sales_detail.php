<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSalesDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('sales_details', function (Blueprint $table) {
        $table->increments('id');
        $table->boolean('is_active');
        $table->integer('book')->unsigned();   
        $table->foreign('book')->references('id')->on('books');
        $table->integer('sales_id')->unsigned();   
        $table->foreign('sales_id')->references('id')->on('sales');
        $table->float('qty',18,2);
        $table->double('price',18,2);
        $table->double('sub_total',18,2);    
    });   
   }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sales_details');
    }
}
