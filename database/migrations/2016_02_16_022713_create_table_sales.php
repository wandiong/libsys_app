<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('sales', function($table)
       {
        $table->increments('id');
        $table->datetime('sales_date');
        $table->boolean('is_active');
        $table->boolean('is_member');
        $table->integer('member')->unsigned()->nullable();
        $table->foreign('member')->references('id')->on('members');
        $table->string('customer');
        $table->integer('payment_method')->unsigned();
        $table->foreign('payment_method')->references('id')->on('payment_methods');
        $table->double('grand_total',18,2);
        $table->timestamps();
    });
   }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sales');
    }
}
