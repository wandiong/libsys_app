<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lending_id')->unsigned();
            $table->foreign('lending_id')->references('id')->on('lendings');
            $table->integer('book')->unsigned();
            $table->foreign('book')->references('id')->on('books');
            $table->datetime('time_start');
            $table->datetime('due_time');
            $table->integer('status')->unsigned();
            $table->foreign('status')->references('id')->on('return_statuses');
            $table->float('total',18,2);
            $table->boolean('is_active');
            $table->timestamps();
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('details');
    }
}
