<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBookDefectList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('book_defect_lists', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('book')->unsigned();   
        $table->foreign('book')->references('id')->on('books');
        $table->float('qty',18,2);
        $table->double('sub_total',18,2);
        $table->boolean('is_active');    
    });   
   }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::drop('book_defect_lists');
    }
}
