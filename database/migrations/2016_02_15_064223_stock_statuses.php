<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Stock_statuses
 *
 * @author  The scaffold-interface created at 2016-02-15 06:42:23am
 * @link  https://github.com/amranidev/scaffold-interfac
 */
class StockStatuses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('stock_statuses',function (Blueprint $table){

        $table->increments('id');
        
        $table->string('code');
        
        $table->string('description');
        
        $table->boolean('is_active');
        
        $table->date('created_at');
        
        $table->date('updated_at');
        
        /**
         * Foreignkeys section
         */
        
        // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('stock_statuses');
     }
}
