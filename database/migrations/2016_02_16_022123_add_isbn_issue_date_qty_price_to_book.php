<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsbnIssueDateQtyPriceToBook extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('books', function($table)
       {
        $table->string('isbn');
        $table->datetime('issue_date');
        $table->integer('qty');
        $table->double('price',18,2);
    });
   }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
