<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'email' => 'nandermind2@gmail.com',
            'password' => bcrypt('drag0000'),
        	'name' => 'wandi',
            'is_active' => true,
            'access_right' => 1,
            'created_at' => '',
            'updated_at' => ''
        ]);
        DB::table('users')->insert([
            'email' => 'nandermind3@gmail.com',
            'password' => bcrypt('drag0000'),
            'name' => 'wandi',
            'is_active' => true,
            'access_right' => 2,
            'created_at' => '',
            'updated_at' => ''
        ]);
        DB::table('users')->insert([
            'email' => 'nandermind4@gmail.com',
            'password' => bcrypt('drag0000'),
            'name' => 'wandi',
            'is_active' => true,
            'access_right' => 3,
            'created_at' => '',
            'updated_at' => ''
        ]);
    }
}
