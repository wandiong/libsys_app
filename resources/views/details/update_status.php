	@extends('layouts.template_superuser')
	@section('header')
	<p>Details - Update</p>
	@endsection
	@section('content')
	{!! Form::open(array('url'=>'/details/update_status','method' => '{{ $method }}'))!!}
	<table class="table table-responsive">
		<tr>
			<td>{!!Form::label('book','Book') !!}</td>
			<td>
				<select name="book" class="form-control" required>
					@foreach($obj as $data)
					<option value={{ $data->Book->id }}>{{$data->Book->book_tittle}}</option>
					@endforeach
				</select>
			</td>
		</tr>
		<tr>
			<td>{!!Form::label('time_start','Time Start') !!}</td>
			<td><input name="time_start" class="datepicker form-control" required type="text"></input></td>
		</tr>
		<tr>
			<td>{!!Form::label('due_time','Due Time') !!}</td>
			<td><input name="due_time" class="datepicker form-control" required type="text"></input></td>	
		</tr>
		<tr>
			<td>{!!Form::label('status','Lending Status') !!}</td>
			<td></td>	
		</tr>
		<tr>
			<td>{!!Form::label('Overdue_Fee','Overdue Fee') !!}</td>
			<td><input name="overdue_fee" id="overdue" class="form-control" required type="text"></input></td>	
		</tr>
	</table>
	@endsection
	@section('content2')
	<table class="table table-responsive">
		<tr>
			<td colspan="2">
				<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save"> Submit</span></button>
			</td>
		</tr>
	</table>
	<script>
		$(function() {
			$( ".datepicker" ).datepicker({dateFormat: "yy-mm-dd"});
		});
	</script>
	{!! Form::close() !!}
	@endsection