	@extends('layouts.template_superuser')
	@section('header')
	<br>
	<p>Details</p>
	@endsection
	@section('content')
	<div>
		@foreach($overview as $overview)
		<table class="table table-responsive" style="width:100%">
			<tr>
				<td>
					@if($overview->is_member == 1)
					<p class="report-head">Customer name / Member code : {{$overview->Member->member_id}}</p>
					@else
					<p class="report-head">Customer name / Member code : {{$overview->customer}}</p>
					@endif
				</td>
				<td>
					<p class="report-head">Payment Method : {{$overview->PaymentMethod->code}}</p>
				</td>
			</tr>
			<tr>
				<td>
					<p class="report-head">Created Date : {{$overview->created_at->format('d/M/y')}}</p>
				</td>
				<td>
					<p class="report-head">Overdue Fee : Rp. {{$overview->total}}</p>
				</td>
			</tr>
		</table>
		@endforeach
	</div>
	<br><br>
	<div>
		<a class="btn btn-success" href="{{url('/lending')}}"> <span class="glyphicon glyphicon-chevron-left"> Return to Lending List</span></a>
		<!-- if the following customer is member -->
		@if($overview->is_member == 1)
			@if($count >= $overview->Member->MemberType->lending_limit)
				<h2> Lending Limit Exceeded, please delete one of the unused details record for continue adding </h2>
			@else
				<a class="btn btn-primary" href="{{ url('/details/new',$overview->id)}}"><span class="glyphicon glyphicon-plus"> Add new Data</span></a>
			@endif
		<!-- if the following customer is normal customer -->
		@elseif($overview->is_member == 0)
			@if($count >= 4)
				<h2> Lending Limit Exceeded, please delete one of the unused details record for continue adding </h2>
			@else
			<a class="btn btn-primary" href="{{ url('/details/new',$overview->id)}}"><span class="glyphicon glyphicon-plus"> Add new Data</span></a>
			@endif
		@endif
		<a class="btn btn-primary" href="{{ url('/details/generate_pdf',$overview->id)}}"><span class="glyphicon glyphicon-book"> Export to PDF</span></a>
	</div>
	<br><br>
	<div>
		<table id="dtb"class="table table-responsive" style="width:100%">
			<thead>
				<tr>
					<td><p class="report-sub-head">Book</p></td>
					<td><p class="report-sub-head">Time Start</p></td>
					<td><p class="report-sub-head">Due Time</p></td>
					<td><p class="report-sub-head">Status</p></td>
					<td><p class="report-sub-head">Overdue Fee</p></td>
					<td><p class="report-sub-head">Action</p></td>
				</tr>
			</thead>
		</div>
		<div>
			@foreach($detail as $list)
			<tr>
				<td><p class="report-content">{{$list->Book->book_tittle}}</p></td>
				<td><p class="report-content">{{$list->time_start->format('d/M/y')}}</p></td>
				<td><p class="report-content">{{$list->due_time->format('d/M/y')}}</p></td>
				<td><p class="report-content">{{$list->ReturnStatus->code}}</p></td>
				<td><p class="report-content">{{$list->total}}</p></td>
				<td>
					<a href="{{ url('/details/drop',$list->id)}}"class="btn btn-warning"><span class="glyphicon glyphicon-remove"> DELETE</span></a>
					<a href="{{ url('/details/updating',$list->id)}}"class="btn btn-success"><span class="glyphicon glyphicon-pencil"> EDIT</span></a>
				</td>
			</tr>
			@endforeach
		</table>
	</div>	
</div>	
@endsection
