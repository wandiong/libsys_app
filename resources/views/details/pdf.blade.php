	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<title>Lending Details Information</title>
		<body>
			<header>
				<center><H1>Lending Details invoice </H1></center>
			</header>
			<style type="text/css">
				.tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; }
				.tg td{font-family:Arial;font-size:12px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
				.tg th{font-family:Arial;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
				.tg .tg-3wr7{font-weight:bold;font-size:12px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
				.tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
				.tg .tg-rv4w{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;}
			</style>

			<div style="font-family:Arial; font-size:12px;">
				<h2>Overview</h2>	
			</div>
			<br>
			@foreach($overview as $overview)
			<table class="table table-responsive" style="width:100%">
				<tr>
					<td>

						@if($overview->is_member == 1)
						<p class="report-head">Customer name / Member code : {{$overview->Member}}</p>
						@else
						<p class="report-head">Customer name / Member code : {{$overview->customer}}</p>
						@endif
					</td>
					<td>
						<p class="report-head">Payment Method : {{$overview->payment_method}}</p>
					</td>
				</tr>
				<tr>
					<td>
						<p class="report-head">Created Date : {{$overview->created_date}}</p>
					</td>
					<td>
						<p class="report-head">Overdue Fee : Rp. {{$overview->fee}}</p>
					</td>
				</tr>
			</table>
			@endforeach
			<div>
				<h2> Details </h2>
			</div>
			<div>
				<table class="table table-responsive" style="width:100%">
					<tr>
						<td><p class="report-sub-head">Book</p></td>
						<td><p class="report-sub-head">Time Start</p></td>
						<td><p class="report-sub-head">Due Time</p></td>
						<td><p class="report-sub-head">Status</p></td>
						<td><p class="report-sub-head">Overdue Fee</p></td>
					</tr>
					@foreach($detail as $list)
					<tr>
						<td><p class="report-content">{{$list->Book->book_tittle}}</p></td>
						<td><p class="report-content">{{$list->time_start->format('d/M/y')}}</p></td>
						<td><p class="report-content">{{$list->due_time->format('d/M/y')}}</p></td>
						<td><p class="report-content">{{$list->ReturnStatus->code}}</p></td>
						<td><p class="report-content">{{$list->total}}</p></td>
					</tr>
					@endforeach
					<tr>
						<td><p class="report-sub-head">Invoice is Issued by</p></td>
					</tr>
					<tr>
						<td><p class="report-content">{{Auth::user()->name}}</p></td>
					</tr>
				</table>
			</div>	
		</table>
	</body>
</head>
</html>