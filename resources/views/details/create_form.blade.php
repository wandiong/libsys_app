	@extends('layouts.template_superuser')
	@section('header')
	<p>Details - CREATE</p>
	@endsection
	@section('content')
	{!! Form::open(array('url'=>'/details/create','method' => '{{ $method }}'))!!}
	@foreach ($lending as $lending)
	{!!form::hidden('lending_id',$lending->id)!!}
	@endforeach
	<table class="table table-responsive">
		<tr>
			<td>{!!Form::label('book','Book') !!}</td>
			<td>
				<select name="book" class="form-control" required>
					@foreach($temp as $books)
					<option value={{ $books->id }}>{{$books->book_tittle}}</option>
					@endforeach
				</select>
			</td>
		</tr>
		<tr>
			<td>{!!Form::label('time_start','Time Start') !!}</td>
			<td><input name="time_start" class="form-control date" required type="text"></input></td>
		</tr>
		<tr>
			<td>{!!Form::label('due_time','Due Time') !!}</td>
			<td><input name="due_time" class="form-control date" required type="text"></input></td>	
		</tr>
	</table>
	@endsection
	@section('content2')
	<table class="table table-responsive">
		<tr>
			<td colspan="2">
				<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save"> Submit</span></button>
			</td>
		</tr>
	</table>
	<script>
	</script>
	{!! Form::close() !!}
	@endsection