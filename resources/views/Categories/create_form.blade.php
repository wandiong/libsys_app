	@extends('layouts.template_superuser')
	@section('header')
		<p>Categories - CREATE</p>
	@endsection
	@section('content')
	{!! Form::open(array('url'=>'/category/insert','method' => '{{ $method }}'))!!}
		<table class="table table-responsive">
			<tr>
					<td>{!!Form::label('code','Code') !!}</td>
					<td>
						<input type="text" required name="code"></input>
					</td>
			</tr>
			<tr>
					<td>{!!Form::label('description','Description') !!}</td>
					<td>
						{!!form::textarea('description')!!}
					</td>
			</tr>
		</table>
	@endsection
	@section('content2')
		<table class="table table-responsive">
			<tr>
				<td colspan="2">
					<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save"> Submit</span></button>
				</td>
			</tr>
		</table>
		{!! Form::close() !!}
	@endsection
