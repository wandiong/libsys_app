	@extends('layouts.template_superuser')
	@section('header')
	<br><br><br>
		<p>Category - Update</p>
	@endsection
	@section('content')
	{!! Form::open(array('url'=>'/category/update','method' => '{{ $method }}'))!!}
		<table class="table table-responsive">
						<tr>
					<td>{!!Form::label('code','Code') !!}</td>
					<td>
						{!!form::text('code',$obj->code)!!}
					</td>
			</tr>
			<tr>
					<td>{!!Form::label('description','Description') !!}</td>
					<td>
						{!!form::textarea('description',$obj->description)!!}
					</td>
			</tr>
					{!!form::hidden('id',$obj->id)!!}
		</table>
	@endsection
	@section('content2')
		<table class="table table-responsive">
			<tr>
				<td colspan="2">
					<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save"> Submit</span></button>
				</td>
			</tr>
		</table>
		{!! Form::close() !!}
	@endsection