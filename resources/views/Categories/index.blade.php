	@extends('layouts.template_superuser')
	@section('header')
	<br>
	<p>LibSys - Category</p>
	@endsection
	@section('content')
	<br><br>
	<div>
		<a class="btn btn-success" href="{{ url('/category/new')}}"><span class="glyphicon glyphicon-plus"> Create</span></a>
	</div>
	<br><br>
	<div>
		<table id="dtb"class="table table-responsive" style="width:100%">
			<thead>
				<tr>
					<th>Code</th>
					<th>Description</th>
					<th>Action</th>
				</tr>
			</thead>
		</div>
		<div>
			<tbody>
				@foreach ($list as $data)
				<tr>
					<td>{{ $data->code }}</td>
					<td>{{ $data->description }}</td>
					<td>
						<a href="{{ url('/category/updating',$data->id)}}"class="btn btn-success"><span class="glyphicon glyphicon-pencil"> EDIT</span></a>
						<a href="{{ url('/category/drop',$data->id)}}"class="btn btn-warning"><span class="glyphicon glyphicon-remove"> Delete </span></a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>	
	@endsection
