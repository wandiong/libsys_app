	@extends('layouts.template_superuser')
	@section('header')
	<br><br><br>
	<p>Books - Update</p>
	@endsection
	@section('content')
	{!! Form::open(array('url'=>'/book/update','method' => '{{ $method }}'))!!}
	<table class="table table-responsive">
		<tr>
			<td>{!!Form::label('isbn','ISBN') !!}</td>
			<td>{!! form::text('isbn',$obj->isbn) !!}</td>
		</tr>
		<tr>
			<td>{!!Form::label('book_tittle','Book Tittle') !!}</td>
			<td>{!! form::text('book_tittle',$obj->book_tittle) !!}
			</td>
		</tr>
		<tr>
			<td>{!!Form::label('book_desc','Book Description') !!}</td>
			<td>{!! form::textarea('book_desc',$obj->book_desc) !!}
			</td>
		</tr>
		<tr>
			<td>{!!Form::label('issue_date','Issue Date') !!}</td>
			<td>
				<input type="text" id="date" class="form-control" name="issue_date" value={{$obj->issue_date}}>
			</td>
		</tr>
		<tr>
			<td>{!!Form::label('categories','Category') !!}</td>
			<td>
				<select name="categories">
					@foreach($temp as $categories)
					<option value={{ $categories->id }}>{{$categories->code}}</option>
					@endforeach
				</select>
			</td>
			{!!form::hidden('id',$obj->id)!!}
		</tr>
		<tr>
			<td>{!!form::label('qty','Qty')!!}</td>
			<td>{!!form::text('qty',$obj->qty)!!}
			</tr>
			<tr>
				<td>{!!form::label('price','Price')!!}</td>
				<td>{!!form::text('price',$obj->price)!!}		
				</tr>
			</table>
			@endsection
			@section('content2')
			<table class="table table-responsive">
				<tr>
					<td colspan="2">
						<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save"> Submit</span></button>
					</td>
				</tr>
			</table>
			{!! Form::close() !!}
			@endsection