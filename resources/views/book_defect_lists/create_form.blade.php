	@extends('layouts.template_superuser')
	@section('header')
	<p>Books - CREATE</p>
	@endsection
	@section('content')
	{!! Form::open(array('url'=>'/book/insert','method' => '{{ $method }}'))!!}
	<table class="table table-responsive">
		<tr>
			<td>{!!Form::label('isbn','ISBN') !!}</td>
			<td>{!! form::text('isbn') !!}</td>
		</tr>
		<tr>
			<td>{!!Form::label('book_tittle','Book Tittle') !!}</td>
			<td>{!! form::text('book_tittle') !!}
			</td>
		</tr>
		<tr>
			<td>{!!Form::label('book_desc','Book Description') !!}</td>
			<td>{!! form::textarea('book_desc') !!}
			</td>
		</tr>
		<tr>
			<td>{!!Form::label('issue_date','Issue Date') !!}</td>
			<td>
				<input type="text" id="date" class="date form-control" name="issue_date">
			</td>
		</tr>
		<tr>
			<td>{!!Form::label('for','For') !!}</td>
			<td>
				<select name="sell">
					<option value=0>Lending</option>
					<option value=1>Sale</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>{!!Form::label('categories','Category') !!}</td>
			<td>
				<select name="categories">
					@foreach($temp as $categories)
					<option value={{ $categories->id }}>{{$categories->code}}</option>
					@endforeach
				</select>
			</td>
		</tr>
		<tr>
			<td>{!!form::label('qty','Qty')!!}</td>
			<td>{!!form::text('qty')!!}
		</tr>
		<tr>
			<td>{!!form::label('price','Price')!!}</td>
			<td>{!!form::text('price')!!}		
		</tr>
	</table>
	@endsection
	@section('content2')
	<table class="table table-responsive">
		<tr>
			<td colspan="2">
				<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save"> Submit</span></button>
			</td>
		</tr>
	</table>
	{!! Form::close() !!}
	@endsection
