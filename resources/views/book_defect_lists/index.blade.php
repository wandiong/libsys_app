	@extends('layouts.template_superuser')
	@section('header')
	<br>
	<p>LibSys - BOOK LIST</p>
	@endsection
	@section('content')
	<br><br>
	<div>
	</div>
	<br>
	<div>
		<a class="btn btn-success" href="{{ url('/book/new')}}"><span class="glyphicon glyphicon-plus"> Create</span></a>
	</div>
	<br>
	<div>
		<table id="dtb" class="table table-striped table-bordered" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>ISBN</th>
					<th>Book Tittle</th>
					<th>Book Description</th>
					<th>issue_date</th>
					<th>Lending/Sale</th>
					<th>Categories</th>
					<th>Qty</th>
					<th>Price</th>
					<th>Entry Date</th>
					<th>Action</th>
				</tr>
			</thead>
		</div>
		<div>
			<tbody>
				@foreach ($list as $data)
				<tr>
					<td>{{ $data->isbn }}</td>
					<td>{{ $data->book_tittle }}</td>
					<td>{{ $data->book_desc }}</td>
					<td>{{ $data->issue_date }}</td>
					<td>
						@if($data->for_sale == 1)
							For Sale
						@else
							For Lending
						@endif
					</td>
					<td>{{ $data->Category->code }}</td>
					<td>{{ $data->qty }}</td>
					<td>{{ $data->price }}</td>
					<td>{{ $data->created_at->format('d/M/y')}}</td>
					<td>
						<a href="{{ url('/book/updating',$data->id)}}"class="btn btn-success"><span class="glyphicon glyphicon-pencil"> EDIT</span></a>
						<a href="{{ url('/book/drop',$data->id)}}"class="btn btn-warning"><span class="glyphicon glyphicon-remove"> DELETE</span></a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>	
	@endsection
