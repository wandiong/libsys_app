	@extends('layouts.template_superuser')
	@section('header')
	<br>
	<p>LibSys - RETURN STATUS</p>
	@endsection
	@section('content')
	<br><br>
	<div>
		<a class="btn btn-success" href="{{ url('/return_status/new')}}"><span class="glyphicon glyphicon-plus">Create</span></a>
	</div>
	<br><br>
	<div>
		<table id="dtb" class="table table-striped table-bordered" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>Code</th>
					<th>Description</th>
					<th>Action</th>
				</tr>
			</thead>
		</div>
		<div>
			@foreach ($list as $data)
			<tr>
				<td>{{ $data->code }}</td>
				<td>{{ $data->description }}</td>
				<td>
					<a href="{{ url('/return_status/updating',$data->id)}}"class="btn btn-success"><span class="glyphicon glyphicon-pencil"> EDIT</span></a>
					<a href="{{ url('/return_status/drop',$data->id)}}"class="btn btn-warning"><span class="glyphicon glyphicon-remove"> Delete </span></a>
				</td>
			</tr>
			@endforeach
		</table>
		{!! $list->render()!!}
	</div>	
	@endsection
