	
@extends('layouts.template_superuser')
@section('header')
<br>
<p>LibSys - AccessRight</p>
@endsection
@section('content')
<br><br>
<div>
	<td><a class="btn btn-success" href="{{ url('/access_right/new')}}"><span class="glyphicon glyphicon-plus"> Create</span></a></td>
</div>
<table id="dtb" class="table table-striped table-bordered" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th>Code</th>
			<th>Description</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($list as $data)
		<tr>
			<td>{{ $data->code }}</td>
			<td>{{ $data->description }}</td>
			<td>
				<a href="{{ url('/access_right/updating',$data->id)}}"class="btn btn-success"><span class="glyphicon glyphicon-pencil"> EDIT</span></a>
				<a href="{{ url('/access_right/drop',$data->id)}}"class="btn btn-warning"><span class="glyphicon glyphicon-remove"> Delete </span></a>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
@endsection