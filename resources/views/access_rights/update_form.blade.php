	@extends('layouts.template_superuser')
	@section('header')
	<br><br><br>
	<p>Access Right - Update</p>
	@endsection
	@section('content')
	{!! Form::open(array('url'=>'/access_right/update','method' => '{{ $method }}'))!!}
	<table class="table table-responsive">
		<tr id="customer">
			<td>
				{!!form::label('Code','Code')!!}
			</td>
			<td>
				{!!form::text('code',$data->code)!!}
			</td>
		</tr>
		<tr>
			<td>
				{!!form::label('desc','Description')!!}
			</td>
			<td>
				{!!form::textarea('description',$data->description)!!}
			</td>
		</tr>
	</table>
	{!!form::hidden('id',$data->id)!!}
	@endsection
	@section('content2')
	<table class="table table-responsive">
		<tr>
			<td colspan="2">
				<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save"> Submit</span></button>
			</td>
		</tr>
	</table>
	{!! Form::close() !!}
	@endsection