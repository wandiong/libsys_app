	@extends('layouts.template_superuser')
	@section('header')
	<br>
	<p>Sales Details</p>
	@endsection
	@section('content')
	<div>
		@foreach($overview as $overview)
		<table class="table table-responsive" style="width:100%">
			<tr>
				<td>
					@if($overview->is_member == 1)
					<p class="report-head">Customer name / Member code : {{$overview->Member->member_id}}</p>
					@else
					<p class="report-head">Customer name / Member code : {{$overview->customer}}</p>
					@endif
				</td>
				<td>
					<p class="report-head">Payment Method : {{$overview->PaymentMethod->code}}</p>
				</td>
			</tr>
			<tr>
				<td>
					<p class="report-head">Sales Date : {{$overview->sales_date}}</p>
				</td>
				<td>
					<p class="report-head">User : {{Auth::user()->name}}</p>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<p class="report-head"> Total : {{$overview->grand_total}}</p>
				</td>
			</tr>
		</table>
		@endforeach
	</div>
	<br><br>
	<div>
		<a class="btn btn-success" href="{{url('/sales')}}"> <span class="glyphicon glyphicon-chevron-left"> Return to Lending List</span></a>
		<a class="btn btn-primary" href="{{ url('/sales_detail/generate_pdf',$overview->id)}}"><span class="glyphicon glyphicon-book"> Export to PDF</span></a>
	</div>
	<div>
		<table class="table table-responsive">
			{!! Form::open(array('url'=>'/sales_detail/create','method' => '{{ $method }}'))!!}
			<tr>
				<p> Input Sold Book </p>
			</tr>
			<tr>
				<td> Book</td>
				<td>
					<select name="book" class="form-control">
						@foreach($book as $book)
						<option value={{$book->id}}>{{ $book->book_tittle }}</option>
						@endforeach
					</select>
					{!! form::hidden('id',$overview->id)!!}
				</td>
			</tr>
			<tr>
				<td> Qty</td>
				<td> <input type="number" class="form-control" name="qty"></td>	
			</tr>
			<tr>
				<td><button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save"> Submit</span></button></td>	
			</tr>
			{!! Form::close()!!}
		</table>
	</div>
	<br><br>
	<div>
		<table id="dtb"class="table table-responsive" style="width:100%">
			<thead>
				<tr>
					<td><p class="report-sub-head">Qty</p></td>
					<td><p class="report-sub-head">Book</p></td>
					<td><p class="report-sub-head">Price</p></td>
					<td><p class="report-sub-head">Total</p></td>
					<td><p class="report-sub-head">Action</p></td>
				</tr>
			</thead>
		</div>
		<div>
			<tbody>
			@foreach($detail as $list)
			<tr>
				<td><p class="report-content">{{$list->qty}}</p></td>
				<td><p class="report-content">{{$list->Book->book_tittle}}</p></td>
				<td><p class="report-content">{{$list->price}}</p></td>
				<td><p class="report-content">{{$list->sub_total}}</p></td>
				<td>
					<a href="{{ url('/sales_detail/drop',$list->id)}}"class="btn btn-warning"><span class="glyphicon glyphicon-remove"> DELETE</span></a>
				</td>
			</tr>
			@endforeach
			</tbody>
		</table>
	</div>	
</div>	
@endsection
