	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<title>Sales Invoice</title>
		<body>
			<header>
				<center><H1>Invoice</H1></center>
			</header>
			<style type="text/css">
				.tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; }
				.tg td{font-family:Arial;font-size:12px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
				.tg th{font-family:Arial;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
				.tg .tg-3wr7{font-weight:bold;font-size:12px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
				.tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
				.tg .tg-rv4w{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;}
			</style>

			<div style="font-family:Arial; font-size:12px;">
				<h2>Overview</h2>	
			</div>
			<br>
			@foreach($overview as $overview)
			<table class="table table-responsive" style="width:100%">
				<tr>
					<td>
						@if($overview->is_member == 1)
						<p class="report-head">Customer name / Member code : {{$overview->Member->member_id}}</p>
						@else
						<p class="report-head">Customer name / Member code : {{$overview->customer}}</p>
						@endif
					</td>
					<td>
						<p class="report-head">Payment Method : {{$overview->PaymentMethod->code}}</p>
					</td>
				</tr>
				<tr>
					<td>
						<p class="report-head"> Total : {{$overview->grand_total}}</p>
					</td>
					<td>
						<p class="report-head">Sales Date : {{$overview->sales_date}}</p><br>
					</td>
				</tr>
			</table>
			@endforeach
			<div>
				<h2> Details </h2>
			</div>
			<div>
				<table id="dtb"class="table table-responsive" style="width:100%">
					<tr>
						<td><p class="report-sub-head">Qty</p></td>
						<td><p class="report-sub-head">Book</p></td>
						<td><p class="report-sub-head">Price</p></td>
						<td><p class="report-sub-head">Total</p></td>
					</tr>
					@foreach($detail as $list)
					<tr>
						<td><p class="report-content">{{$list->qty}}</p></td>
						<td><p class="report-content">{{$list->Book->book_tittle}}</p></td>
						<td><p class="report-content">{{$list->price}}</p></td>
						<td><p class="report-content">{{$list->sub_total}}</p></td>
					</tr>
					@endforeach
					<tr>
						<td><p class="report-content">Total</p></td>
						<td></td>
						<td></td>
						<td><p class="report-content">{{$overview->grand_total}}</td>
					</tr>
				</table>
			</div>
			<br>
			<br>
			<div>
				Best Regards<br><br><br><br>
				{{Auth::user()->name}}
			</div>	
		</table>

	</body>
</head>
</html>