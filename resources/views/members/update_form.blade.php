	@extends('layouts.template_superuser')
	@section('header')
	<br><br><br>
		<p>MEMBER - Update</p>
	@endsection
	@section('content')
	{!! Form::open(array('url'=>'/member/update','method' => '{{ $method }}'))!!}
		<table class="table table-responsive">
			<tr>
					<td>{!!Form::label('member_code','Member ID') !!}</td>
					<td>{!! form::text('member_code',$objMember->member_id,array_merge(['class' => 'disabled','readonly','required']))!!}
					</td>
			</tr>
			<tr>
					<td>{!!Form::label('member_type','Member Type') !!}</td>
					<td>
						<select name="member_type">
						@foreach($objMemberType as $member_type)
							<option value={{ $member_type->id }}>{{$member_type->code}}</option>
						@endforeach
						</select>
					</td>
					{!!form::hidden('id',$objMember->id)!!}
			</tr>
		</table>
	@endsection
	@section('content2')
		<table class="table table-responsive">
			<tr>
				<td colspan="2">
					<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save"> Submit</span></button>
				</td>
			</tr>
		</table>
		{!! Form::close() !!}
	@endsection