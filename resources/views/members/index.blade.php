	@extends('layouts.template_superuser')
	@section('header')
	<br>
	<p>LibSys - MEMBER LIST</p>
	@endsection
	@section('content')
	<br><br>
	<div>
		<a class="btn btn-success" href="{{ url('/member/new')}}"><span class="glyphicon glyphicon-plus">Create</span></a>
	</div>
	<br><br>
	<div>
		<table id="dtb" class="table table-responsive" style="width:100%">
			<thead>
				<tr>
					<th>Member ID</th>
					<th>Member Type</th>
					<th>Action</th>
				</tr>
			</thead>
		</div>
		<div>
			<tbody>
				@foreach ($list as $data)
				<tr>
					<td>{{ $data->member_id }}</td>
					<td>{{ $data->member_type }}</td>
					<td>
						<a href="{{ url('/member/updating',$data->id)}}"class="btn btn-success"><span class="glyphicon glyphicon-pencil"> EDIT</span></a>
						<a href="{{ url('/member/drop',$data->id)}}"class="btn btn-warning"><span class="glyphicon glyphicon-remove"> DELETE</span></a>
					</td>
					@endforeach
				</tr>
			</tbody> 
		</table>
	</div>	
	@endsection
