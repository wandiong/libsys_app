	@extends('layouts.template_superuser')
	@section('header')
	<br>
	<p>LibSys - USER LIST</p>
	@endsection
	@section('content')
	<br><br>
	<div>
		<a class="btn btn-success" href="{{ url('/user/new')}}"><span class="glyphicon glyphicon-plus">Create</span></a>
	</div>
	<br><br>
	<div>
		<table id="dtb" class="table table-responsive" style="width:100%">
			<thead>
				<tr>
					<th>Email</th>
					<th>Name</th>
					<th>Status</th>
					<th>Access Right</th>
					<th>Modify Access Right</th>
					<th>Action</th>
				</tr>
			</thead>
				</div>
				<div>
					@foreach ($list as $data)
					<tr>
						<td>{{ $data->name }}</td>
						<td>{{ $data->email }}</td>
						<td>
							@if($data->status == 1)
							{{ "Active" }}
							@else
							{{ "Innactive" }}
							@endif
						</td>
						<td>{{ $data->AccessRight->code}}</td>
						<td>
							{!! Form::open(array('url'=>'/user/bulkupdate','method' => '{{ $method }}'))!!}
							<select name="access_right">
								@foreach($temp as $arData)
								<option value={{$arData->id}}>{{ $arData->code }}</option>
								@endforeach
							</select>
							{!! form::hidden('id',$data->id)!!}
							<br><br>
							<button type="submit"><span class="glyphicon glyphicon-pencil"> Change</span></a>	
								{!! Form::close() !!}
							</td>
							<td>
								<a href="{{ url('/user/updating',$data->id)}}"class="btn btn-success"><span class="glyphicon glyphicon-pencil"> EDIT</span></a>
								@if ($data->status == 1)
								<a href="{{ url('/user/drop',$data->id)}}"class="btn btn-warning"><span class="glyphicon glyphicon-remove"> Innactivate </span></a>
								@else
								<a href="{{ url('/user/drop',$data->id)}}"class="btn btn-primary"><span class="glyphicon glyphicon-pencil"> Activate </span></a>
								@endif
							</td>
						</tr>
						@endforeach
					</table>
					{!! $list->render()!!}
				</div>	
				@endsection
