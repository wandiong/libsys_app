	@extends('layouts.template_superuser')
	@section('header')
	<br>
	<p>LibSys - Sales</p>
	@endsection
	@section('content')
	<br><br>
	<div>
		<a class="btn btn-success" href="{{ url('/sales/new')}}"><span class="glyphicon glyphicon-plus">Create</span></a>
	</div>
	<br><br>
	<div>
		<table id="dtb" class="table table-responsive" style="width:100%">
			<thead>
				<th>Sales Date</th>
				<th>Customer Name/Member Code</th>
				<th>Payment Method</th>
				<th>total</th>
				<th>Action</th>
			</thead>
		</div>
		<div>
			<tbody>
				@foreach ($list as $data)
				<tr>
					<td>{{ $data->sales_date}}</td>
					<td>
						@if($data->is_member == 1)
						{{$data->Member->member_id}}
						@else
						{{$data->customer}}
						@endif
					</td>
					<td>
						{{ $data->PaymentMethod->code }}
					</td>
					<td>
						{{ $data->grand_total }}
					</td>
					<td>
						<a href="{{ url('/sales_detail',$data->id)}}" class="btn btn-primary"><span class="glyphicon glyphicon-search"> View</span></a>
						
						<a href="{{ url('/sales/updating',$data->id)}}"class="btn btn-success"><span class="glyphicon glyphicon-pencil"> EDIT</span></a>

						<a href="{{ url('/sales/drop',$data->id)}}"class="btn btn-warning"><span class="glyphicon glyphicon-remove"> Delete </span></a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>	
	@endsection
