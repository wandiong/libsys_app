	@extends('layouts.template_superuser')
	@section('header')
	<p>Sales - CREATE</p>
	@endsection
	@section('content')
	{!! Form::open(array('url'=>'/sales/insert','method' => '{{$method}}'))!!}
	<table class="table table-responsive">
		<tr>
			<td>{!!Form::label('sales_date','Sales Date') !!}</td>
			<td><input type="text" name="sales_date" class="date form-control" ></td>
		</tr>
		<tr>
			<td>{!!Form::label('customerstat','Customer Status') !!}</td>
			<td>
				<select name="is_member" id="member_status">
					<option value="asdasdasdsadsdsd"></option>
					<option value="0">Customer</option>
					<option value="1">Member</option>
				</select>
			</td>
		</tr>
		<tr style="display:none" id="member">
			<td>{!!Form::label('member_id','Member ID') !!}</td>
			<td>
				<select name="member_id">
					@foreach($member as $member)
					<option value={{$member->id}}>{{$member->member_id}}</option>
					@endforeach
				</select>
			</td>
		</tr>
		<tr style="display:none" id="customer">
			<td>
				{!!form::label('customer','Customer Name')!!}
			</td>
			<td>
				{!!form::text('customer')!!}
			</td>
		</tr>
		<tr>
			<td>
				{!!form::label('Payment_Method','Payment Method')!!}
			</td>
			<td>
				<select name="payment_method">
					@foreach($payment_method as $pm)
					<option value={{$pm->id}}>{{$pm->code}}</option>
					@endforeach
				</select>
			</td>
		</tr>
	</table>
	<script>
		$("#member_status").change(function(){
			var temp = $("#member_status").find('option:selected').val();
			if(temp == 1)
			{
				$("#member").show();
				$("#customer").hide();
			}else if (temp == 0){
				$("#member").hide();
				$("#customer").show();					
			}else{
				$("#member").hide();
				$("#customer").hide();					
			}
		});
	</script>
	@endsection
	@section('content2')
	<table class="table table-responsive">
		<tr>
			<td colspan="2">
				<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save"> Submit</span></button>
			</td>
		</tr>
	</table>
	{!! Form::close() !!}
	@endsection
