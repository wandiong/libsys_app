<html>
<head>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<style>
		a{
			color:black;
		}
		body{
			background-color: gray;
		}
		table{
			border-color:white;
		}
		p{
			font-size: 30px;
		}
	</style>
</head>
<title>LibSys-Login</title>
<body class="container">
	<br><br>
	@yield("header")
	<br>
	@if (Session::has('flash_notification.message'))
	<div class="alert alert-{{ Session::get('flash_notification.level') }}">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

		{{ Session::get('flash_notification.message') }}
	</div>
	@endif
	<table class="table table-bordered table-responsive">
		@yield("content")
	</table>
	<table class="table table-responsive">
		@yield("content2")
	</table>
	@yield("footer")
</body>
</html>