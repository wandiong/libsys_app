  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="stylesheet" href="{{URL::asset('bootstrap/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{URL::asset('dist/css/AdminLTE.min.css')}}">
  <script src="{{URL::asset('plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>
  <script src="{{URL::asset('bootstrap/js/bootstrap.min.js')}}"></script>
  <script src="{{URL::asset('dist/js/app.min.js')}}"></script>
  <link rel="stylesheet" href="{{URL::asset('dist/css/skins/skin-blue.min.css')}}">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
  <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <!-- page script -->
  <style>
    thead{
      background-color: #ADD8E6;
    }
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    .row.content {height: 450px}

    .sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
    }

    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }

    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;} 
    }
    p{
      font-size: 30px;
      font-family: "Comic Sans MS";
    }
    .disabled{
      background: #dddddd;

    }
    .report-head{
      font-size: 25px;
      font-family: "Times New Roman";
    }
    .report-sub-head{
      font-size: 20px;
      font-family: "Times New Roman";
    }
    .report-content{
      font-size: 20px;
      font-family: "Times New Roman";
    }
  </style>
  <script>
    $(document).ready(function() {
      $('#dtb').DataTable();
      $( ".date" ).datepicker({dateFormat: "yy-mm-dd"});
    });
  </script>