	@extends('layouts.template_superuser')
	@section('header')
		<p>MEMBER TYPE - CREATE</p>
	@endsection
	@section('content')
	{!! Form::open(array('url'=>'/member_type/insert','method' => '{{ $method }}'))!!}
		<table class="table table-responsive">
			<tr>
					<td>{!!Form::label('code','Code') !!}</td>
					<td>
						<input type="text" required name="code"></input>
					</td>
			</tr>
			<tr>
					<td>{!!Form::label('description','Description') !!}</td>
					<td>
						{!!form::textarea('description')!!}
					</td>
			</tr>
			<tr>
					<td>{!!Form::label('member_price','Member Price') !!}</td>
					<td>
						<input type="text" required name="member_price"></input>
					</td>
			</tr>
			<tr>
					<td>{!!Form::label('lending_limit','Lending Limit') !!}</td>
					<td>
						<input type="text" required name="lending_limit"></input>
					</td>
			</tr>
			
		</table>
	@endsection
	@section('content2')
		<table class="table table-responsive">
			<tr>
				<td colspan="2">
					<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save"> Submit</span></button>
				</td>
			</tr>
		</table>
		{!! Form::close() !!}
	@endsection