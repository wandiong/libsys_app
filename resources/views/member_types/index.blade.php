	@extends('layouts.template_superuser')
	@section('header')
	<br>
	<p>LibSys - MEMBER TYPE</p>
	@endsection
	@section('content')
	<br><br>
	<div>
		<a class="btn btn-success" href="{{ url('/member_type/new')}}"><span class="glyphicon glyphicon-plus">Create</span></a>
	</div>
	<br><br>
	<div>
	<table id="dtb" class="table table-striped table-bordered" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>Code</th>
							<th>Description</th>
							<th>member Overdue Fee</th>
							<th>Lending Limit</th>
							<th>Action</th>
						</tr>
					</thead>
				</thead>
			</div>
			<div>
				@foreach ($list as $data)
				<tr>
					<td>{{ $data->code }}</td>
					<td>{{ $data->description }}</td>
					<td>{{ $data->member_price }}</td>
					<td>{{ $data->lending_limit}}</td>
					<td>
						<a href="{{ url('/member_type/updating',$data->id)}}"class="btn btn-success"><span class="glyphicon glyphicon-pencil"> EDIT</span></a>
						<a href="{{ url('/member_type/drop',$data->id)}}"class="btn btn-warning"><span class="glyphicon glyphicon-remove"> Delete </span></a>
					</td>
				</tr>
				@endforeach
			</table>
		</div>	
		@endsection
