	@extends('layouts.template_superuser')
	@section('header')
	<br><br><br>
	<p>MEMBER TYPE - Update</p>
	@endsection
	@section('content')
	{!! Form::open(array('url'=>'/member_type/update','method' => '{{ $method }}'))!!}
	<table class="table table-responsive">
		<tr>
			<td>{!!Form::label('code','Code') !!}</td>
			<td>
				{!!form::text('code',$obj->code)!!}
			</td>
		</tr>
		<tr>
			<td>{!!Form::label('description','Description') !!}</td>
			<td>
				{!!form::textarea('description',$obj->description)!!}
			</td>
		</tr>
		<tr>
			<td>{!!Form::label('member_price','Member Price') !!}</td>
			<td>
				{!! form::text('member_price',$obj->member_price) !!}
			</td>
		</tr>
		<tr>
			<td>{!!Form::label('lending_limit','Lending Limit') !!}</td>
			<td>
			<input type="text" required name="lending_limit" value={{$obj->lending_limit}}></input>
			</td>
		</tr>
		{!!form::hidden('id',$obj->id)!!}
	</table>
	@endsection
	@section('content2')
	<table class="table table-responsive">
		<tr>
			<td colspan="2">
				<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save"> Submit</span></button>
			</td>
		</tr>
	</table>
	{!! Form::close() !!}
	@endsection