	@extends('layouts.template_superuser')
	@section('header')
	<br>
	<p>LibSys - Lendings</p>
	@endsection
	@section('content')
	<br><br>
	<div>
		<a class="btn btn-success" href="{{ url('/lending/new')}}"><span class="glyphicon glyphicon-plus">Create</span></a>
	</div>
	<br><br>
	<div>
		<table id="dtb" class="table table-responsive" style="width:100%">
			<thead>
				<th>Input Date</th>
				<th>Customer Name/Member Code</th>
				<th>Payment Method</th>
				<th>total</th>
				<th>Action</th>
			</thead>
		</div>
		<div>
			<tbody>
				@foreach ($list as $data)
				<tr>
					<td>{{ $data->created_at->format('d/M/y') }}</td>
					<td>
						@if($data->is_member == 1)
						{{$data->Member->member_id}}
						@else
						{{$data->customer}}
						@endif
					</td>
					<td>
						{{ $data->PaymentMethod->code }}
					</td>
					<td>
						{{ $data->total }}
					</td>
					<td>
						<a href="{{ url('/details',$data->id)}}" class="btn btn-primary"><span class="glyphicon glyphicon-search"> View</span></a>
						
						<a href="{{ url('/lending/updating',$data->id)}}"class="btn btn-success"><span class="glyphicon glyphicon-pencil"> EDIT</span></a>

						<a href="{{ url('/lending/drop',$data->id)}}"class="btn btn-warning"><span class="glyphicon glyphicon-remove"> Delete </span></a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>	
	@endsection
