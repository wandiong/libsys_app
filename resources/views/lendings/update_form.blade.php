	@extends('layouts.template_superuser')
	@section('header')
	<p>Lending - Update</p>
	@endsection
	@section('content')
	{!! Form::open(array('url'=>'/lending/update','method' => '{{$method}}'))!!}
	<table class="table table-responsive">
		@if($data->is_member == 1)
		<tr id="member">
			<td>{!!Form::label('member_id','Member ID') !!}</td>
			<td>
				<select name="member_code">
					@foreach($temp as $member)
					<option value={{$member->id}}>{{$member->member_id}}</option>
					@endforeach
				</select>
			</td>
		</tr>
		@else
		<tr id="customer">
			<td>
				{!!form::label('customer','Customer Name')!!}
			</td>
			<td>
				{!!form::text('customer',$data->customer)!!}
			</td>
		</tr>
		@endif
		<tr>
			<td>
				{!!form::label('Payment_Method','Payment Method')!!}
			</td>
			<td>
				<select name="payment_method">
					@foreach($pm as $pm)
					<option value={{$pm->id}}>{{$pm->code}}</option>
					@endforeach
				</select>
			</td>
		</tr>
		<tr>
			<td>
				{!!form::label('total','Total')!!}
			</td>
			<td>
				{!!form::text('total',$data->total)!!}
			</td>
		</tr>
	</table>
	{!!form::hidden('id',$data->id)!!}
	@endsection
	@section('content2')
	<table class="table table-responsive">
		<tr>
			<td colspan="2">
				<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save"> Submit</span></button>
			</td>
		</tr>
	</table>
	{!! Form::close() !!}
	@endsection
